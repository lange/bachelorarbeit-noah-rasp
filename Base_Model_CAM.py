import matplotlib.pyplot as plt
import matplotlib
import pandas as pd
import numpy as np
import tensorflow as tf

from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.layers.experimental import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix

seed = 202
tf.random.set_seed(202)

plt.rc('figure', autolayout=True)
plt.rc('axes', labelweight='bold', titleweight='bold', titlesize=20)

bee_csv = pd.read_csv(r"E:\Uni\Informatik\Bachelorarbeit\Data\bee_data.csv")
healthOneHot = pd.get_dummies(bee_csv['health'], drop_first=False)

bee_data = []
#health_amounts = bee_csv['health'].value_counts().plot(kind='bar')
#health_amounts.set_title('Value distribution in the Dataset')
#plt.show()

heatmap_targets = []

for i in range(len(bee_csv['health'])):
    picture = tf.io.read_file(r"E:\Uni\Informatik\Bachelorarbeit\Data\bee_imgs\bee_imgs"+"\\"+bee_csv['file'][i])
    picture = tf.io.decode_png(picture, channels=3)
    picture = tf.image.convert_image_dtype(picture, dtype=tf.float32)
    picture = tf.image.resize(picture, (128, 128))
    picture = tf.image.rgb_to_grayscale(picture)
    picture = picture.numpy()
    bee_data.append(picture)
    if bee_csv['health'][i] == "missing queen":
        heatmap_targets.append(picture)

bee_pictures = np.stack(bee_data)
heatmap_targets = np.stack(heatmap_targets)

X_train, X_hold, y_train, y_hold = train_test_split(bee_pictures, healthOneHot, test_size=0.3, random_state=202, stratify=healthOneHot)
X_val, X_test, y_val, y_test = train_test_split(X_hold, y_hold, test_size=1/3, random_state=202, stratify=y_hold)

model = keras.Sequential([
    #Preprocessing
    preprocessing.RandomContrast(1.0, seed=202),
    preprocessing.RandomFlip('horizontal', seed=202),
    #Model Base
    layers.Conv2D(filters=32, kernel_size=3, activation='relu', padding='same', input_shape=[128, 128, 1]),
    layers.MaxPool2D(strides=(2, 2), padding='same'),
    layers.Conv2D(filters=64, kernel_size=3, activation='relu', padding='same'),
    layers.MaxPool2D(strides=(2, 2), padding='same'),
    layers.Conv2D(filters=128, kernel_size=3, activation='relu', padding='same'),
    layers.MaxPool2D(strides=(2, 2), padding='same'),

    #Model Head
    layers.GlobalAvgPool2D(),
    layers.Dropout(0.3, seed=202),
    layers.Dense(units=128, activation='relu'),
    layers.Dense(units=256, activation='relu'),
    layers.Dense(units=6, activation='softmax')
])

earlyStopping = EarlyStopping(
    min_delta=0.002,
    patience=30,
    restore_best_weights=True
)

model.compile(
    optimizer=tf.keras.optimizers.Adam(),
    loss='categorical_crossentropy',
    metrics=['accuracy', tf.keras.metrics.Precision(), tf.keras.metrics.Recall()]
)

train_hist = model.fit(
    X_train,
    y_train,
    validation_data=(X_val, y_val),
    epochs=500,
    callbacks=[earlyStopping]
)
prediction = model.evaluate(X_test, y_test)
accuracyplot = pd.DataFrame(train_hist.history).loc[: ,['accuracy', 'val_accuracy']].plot()
recallplot = pd.DataFrame(train_hist.history).loc[: ,['recall', 'val_recall', 'precision', 'val_precision']].plot()
plt.show()

confusion_predictions = model.predict(bee_pictures)
print(confusion_matrix(healthOneHot.to_numpy().argmax(axis=1), confusion_predictions.argmax(axis=1)))

preGAP_output = model.get_layer("conv2d_2").output
dense2 = model.get_layer("dense_2").output

for i in range(len(heatmap_targets)):
    heatmap = keras.models.Model([model.inputs], [preGAP_output, dense2])
    with tf.GradientTape() as tape:
        image = heatmap_targets[i]
        imageexp = np.expand_dims(image, axis=0)
        (conv, pred) = heatmap(imageexp)
        loss = pred[:, 5]

    gradients = tape.gradient(loss, conv)
    convCast = tf.cast(conv > 0, "float32")
    gradientsCast = tf.cast(gradients > 0, "float32")
    guidedGradients = gradients * gradientsCast * convCast
    weights = tf.reduce_mean(guidedGradients[0], axis=(0, 1))
    CAM = tf.reduce_sum(tf.multiply(weights, convCast[0]), axis=-1)
    CAM = tf.squeeze(CAM)
    CAM = tf.maximum(CAM, 0) / tf.math.reduce_max(CAM)
    plt.matshow(CAM, cmap='jet')
    plt.show()
    heatmap = np.uint8(255 * CAM)
    jet = matplotlib.cm.get_cmap("jet")
    jet_colors = jet(np.arange(256))[:, :3]
    coloured_heatmap = jet_colors[heatmap]
    finished_heatmap = tf.image.resize(coloured_heatmap, (128, 128))
    finished_heatmap = tf.squeeze(finished_heatmap)
    plt.imshow(image)
    plt.imshow(finished_heatmap, alpha=0.5)
    plt.show()

