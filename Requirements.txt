The libraries listed have required dependencies, that can be automatically installed by an environment manager.
Conda was used here. 
The version numbers listed were used to run the code.
Tensorflow_addons produces a warning at the start but the execution of the code works regardless.

Python			3.9.12
Keras			2.6.0
Tensorflow		2.6.0
Tensorflow_addons	0.13.0
Pandas			1.4.2
Numpy 			1.21.5	
Matplotlib		3.5.2	
Sklearn			1.0.2
Imblearn		0.7.0
